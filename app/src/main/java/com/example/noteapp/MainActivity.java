package com.example.noteapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Attributes;

public class MainActivity extends AppCompatActivity {
  static ArrayList<String> notes = new ArrayList<>();
  static ArrayAdapter arrayAdapter;
  static Set<String> set;
    private Object ArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferences sharedPreferences = this.getSharedPreferences("com.example.noteapp", Context.MODE_PRIVATE);
         ListView listView = (ListView) findViewById(R.id.listView);
        set = sharedPreferences.getStringSet("notes", null);
        notes.clear();
        if (set != null) {
            notes.addAll(set);
        }else {
            notes.add("Γραψε σημείωση");
            set = new HashSet<String>();
            set.addAll(notes);
            sharedPreferences.edit().remove("notes").apply();
            sharedPreferences.edit().putStringSet("notes", set).apply();
        }


        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, notes);

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), EditNote.class);
                intent.putExtra("noteId", position);
                startActivity(intent);
            };

        });

        //sharedPreferences.edit().putString("Name", "Alex").apply();
        //String name = sharedPreferences.getString("Name", "");
        //Log.i("Name: ", name);
listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        new AlertDialog.Builder(MainActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Να διαγραφεί?")
                .setMessage("Διαγραφή?")
                .setPositiveButton("ΝΑΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        notes.remove(position);
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences( "com.example.noteapp",Context.MODE_PRIVATE);
                        if (set == null) {
                            set = new HashSet<String>();
                            {set.clear();
                            }
                        }
                        set.addAll(notes);
                        sharedPreferences.edit().remove("notes").apply();
                        sharedPreferences.edit().putStringSet("notes", set).apply();
                        arrayAdapter.notifyDataSetChanged();

                    }


                })
                .setNegativeButton("OXI",null)
                .show();
    }
});

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notes.add(" ");

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences( "com.example.noteapp",Context.MODE_PRIVATE);
                if (set == null) {
                    set = new HashSet<String>();
                    {set.clear();
                    }
                }
                set.addAll(notes);
                sharedPreferences.edit().remove("notes").apply();
                sharedPreferences.edit().putStringSet("notes", set).apply();
                arrayAdapter.notifyDataSetChanged();
                Intent intent = new Intent(getApplicationContext(), EditNote.class);
                intent.putExtra("notesId", notes.size() -1);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                   //     .setAction("Action", null).show();
                startActivity(intent);
            }



        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
